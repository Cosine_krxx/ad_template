#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import sys

if sys.getdefaultencoding() != 'utf-8':
  reload(sys)
  sys.setdefaultencoding('utf-8')


RootDir = os.path.dirname(os.path.realpath(__file__))
ExportDir = RootDir + '/Export'

exportBootPath = RootDir + '/Export/Default/index_Boot.html'
exportNoBootPath = RootDir + '/Export/Default/index_NoBoot.html'
exportWithMraidTagPath = RootDir + '/Export/Default/index_Mraid.html'
exportNoCompressNoBootPath = RootDir + '/Export/Default/index_NoBoot_NoCompress.html'
exportFbHtmlCompressionPath = RootDir + '/Export/Default/index-compression-template-fb.html'
exportFbJsCompressionPath = RootDir + '/Export/Default/launcher-fb.js'

DefaultPath = RootDir + '/Main/Default/compile.py ' + RootDir

FacebookPath = RootDir + '/Main/Facebook/compile.py ' + RootDir
GooglePath = RootDir + '/Main/Google/compile.py ' + RootDir
IronSourcePath = RootDir + '/Main/IronSource_dapi/compile.py ' + RootDir

MintegralPath = RootDir + '/Main/Mintegral/compile.py ' + RootDir
MraidPath = RootDir + '/Main/Mraid/compile.py ' + RootDir
TiktokPath = RootDir + '/Main/Tiktok/compile.py ' + RootDir

ApplovinPath = RootDir + '/Main/Applovin/compile.py ' + RootDir
UnityPath = RootDir + '/Main/Unity/compile.py ' + RootDir
MaioPath = RootDir + '/Main/Maio/compile.py ' + RootDir

TapjoyPath = RootDir + '/Main/Tapjoy/compile.py ' + RootDir
NendPath = RootDir + '/Main/Nend/compile.py ' + RootDir
AdcolonyPath = RootDir + '/Main/Adcolony/compile.py ' + RootDir
AppliftPath = RootDir + '/Main/Applift/compile.py ' + RootDir

ZucksPath = RootDir + '/Main/Zucks/compile.py ' + RootDir

def getCocosVersion():
  BuildDir = os.path.abspath(os.path.join(RootDir, os.pardir))
  engineScrPath = BuildDir + '/build/web-mobile/cocos2d-js-min.js'
  file_object = open(engineScrPath)
  engineStr = file_object.read()

  if "ENGINE_VERSION=" in engineStr:
    v_index = engineStr.rfind("ENGINE_VERSION")
    version = engineStr[v_index+16: v_index+16+5]+"\n"
    print("Cocos Creator v"+version)
    version = version.replace(".", "", 2)
  else:
    version = "0"
    raise BaseException("version not Found")

  versionInt = int(version)
  return versionInt

def start():
  if not os.path.exists(ExportDir):
    os.mkdir(ExportDir)

  if(getCocosVersion() < 240):
    raise BaseException("Cocos version < 2.4.0, please use older tool")

  # Important Source. Do not disable.
  os.system(DefaultPath)


  # Platform Sources.
  os.system(FacebookPath)
  os.system(GooglePath)
  os.system(IronSourcePath)

  os.system(MintegralPath)
  os.system(MraidPath)
  os.system(TiktokPath)

  os.system(ApplovinPath) 
  os.system(UnityPath)
  
  os.system(MaioPath)
  os.system(TapjoyPath)

  os.system(NendPath)
  os.system(AdcolonyPath)
  os.system(AppliftPath)

  os.system(ZucksPath)

  #remove
  os.remove(exportBootPath)
  os.remove(exportNoBootPath)
  os.remove(exportNoCompressNoBootPath)
  os.remove(exportFbHtmlCompressionPath)
  os.remove(exportFbJsCompressionPath)

  print('-> Export End !!') 

if __name__ == '__main__':
  start()