var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
    if(!isMobile){
      var splash = document.getElementById('splash');
      splash.style.transform = 'rotate(0deg)';

      var myW = window;
      if ( myW.location === myW.parent.location ) {
        // The page is in an iframe
        splash.style.left = '20%';
      }
    }