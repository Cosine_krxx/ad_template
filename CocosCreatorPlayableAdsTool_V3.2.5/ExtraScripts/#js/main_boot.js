window.boot = function () {
    var settings = window._CCSettings;
    window._CCSettings = undefined;
    var onProgress = null;

    let { RESOURCES, INTERNAL, MAIN, START_SCENE } = cc.AssetManager.BuiltinBundleName;
    function setLoadingDisplay() {
        // Loading splash scene
        var splash = document.getElementById('splash');
        var progressBar = splash.querySelector('.progress-bar span');
        onProgress = function (finish, total) {
            var percent = 100 * finish / total;
            if (progressBar) {
                progressBar.style.width = percent.toFixed(2) + '%';
            }
        };
        splash.style.display = 'block';
        progressBar.style.width = '0%';

        cc.director.once(cc.Director.EVENT_AFTER_SCENE_LAUNCH, function () {
            splash.style.display = 'none';
        });
    }

    var onStart = function () {

        cc.view.enableRetina(true);
        cc.view.resizeWithBrowserSize(true);

        if (cc.sys.isBrowser) {
            setLoadingDisplay();
        }

        if (cc.sys.isMobile) {
            if (settings.orientation === 'landscape') {
                cc.view.setOrientation(cc.macro.ORIENTATION_LANDSCAPE);
            }
            else if (settings.orientation === 'portrait') {
                cc.view.setOrientation(cc.macro.ORIENTATION_PORTRAIT);
            }
            // cc.view.enableAutoFullScreen([
            //     cc.sys.BROWSER_TYPE_BAIDU,
            //     cc.sys.BROWSER_TYPE_BAIDU_APP,
            //     cc.sys.BROWSER_TYPE_WECHAT,
            //     cc.sys.BROWSER_TYPE_MOBILE_QQ,
            //     cc.sys.BROWSER_TYPE_MIUI,
            // ].indexOf(cc.sys.browserType) < 0);
        }

        // Limit downloading max concurrent task to 2,
        // more tasks simultaneously may cause performance draw back on some android system / browsers.
        // You can adjust the number based on your own test result, you have to set it before any loading process to take effect.
        if (cc.sys.isBrowser && cc.sys.os === cc.sys.OS_ANDROID) {
            cc.assetManager.downloader.maxConcurrency = 2;
            cc.assetManager.downloader.maxRequestsPerFrame = 2;
        }

        var launchScene = settings.launchScene;
        var bundle = cc.assetManager.bundles.find(function (b) {
            return b.getSceneInfo(launchScene);
        });

        bundle.loadScene(launchScene, null, onProgress,
            function (err, scene) {
                if (!err) {
                    cc.director.runSceneImmediate(scene);
                    if (cc.sys.isBrowser) {
                        // show canvas
                        var canvas = document.getElementById('GameCanvas');

                        canvas.style.visibility = '';
                        var div = document.getElementById('GameDiv');
                        if (div) {
                            div.style.backgroundImage = '';
                        }
                        console.log('Success to load scene: ' + launchScene);
                    }
                }
            }
        );

    };

    var option = {
        id: 'GameCanvas',
        debugMode: settings.debug ? cc.debug.DebugMode.INFO : cc.debug.DebugMode.ERROR,
        showFPS: settings.debug,
        frameRate: 60,
        groupList: settings.groupList,
        collisionMatrix: settings.collisionMatrix,
    };

    cc.assetManager.init({
        bundleVers: settings.bundleVers,
        remoteBundles: settings.remoteBundles,
        server: settings.server
    });

    let bundleRoot = [INTERNAL, MAIN];
    settings.hasStartSceneBundle && bundleRoot.push(START_SCENE);
    settings.hasResourcesBundle && bundleRoot.push(RESOURCES);

    var count = 0;
    function cb(err) {
        if (err) return console.error(err.message, err.stack);
        count++;
        if (count === bundleRoot.length + 1) {
            cc.game.run(option, onStart);

            if (settings.orientation === 'landscape') {
                cc.sys.isMobile = true;
                cc.view._resizeEvent(CC_JSB);
                cc.view._orientationChange();
            }
        }
    }


    settings.jsList = [];
    cc.assetManager.loadScript(settings.jsList.map(function (x) { return 'src/' + x; }), cb);

    for (let i = 0; i < bundleRoot.length; i++) {
        cc.assetManager.loadBundle(bundleRoot[i], cb);
    }
};



//Extra
if (window.document) {
    const REGEX = /^\w+:\/\/.*/;

    var __audioSupport = cc.sys.__audioSupport;
    var formatSupport = __audioSupport.format;
    var context = __audioSupport.context;


    // 转二进制Blob
    function base64toBlob(base64, type) {
        // 将base64转为Unicode规则编码
        var bstr = atob(base64, type),
            n = bstr.length,
            u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n) // 转换编码后才可以使用charCodeAt 找到Unicode编码
        }
        return new Blob([u8arr], {
            type: type,
        })
    }

    // 转二进制
    function base64toArray(base64) {
        // 将base64转为Unicode规则编码
        var bstr = atob(base64),
            n = bstr.length,
            u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n) // 转换编码后才可以使用charCodeAt 找到Unicode编码
        }

        return u8arr;
    }

    function downloadDomAudio(url, options, onComplete) {
        var dom = document.createElement('audio');

        var data = window.resMap[url.split("?")[0]];
        data = base64toBlob(data, "audio/mpeg");

        if (window.URL) {
            dom.src = window.URL.createObjectURL(data);
        } else {
            dom.src = data;
        }

        var clearEvent = function () {
            clearTimeout(timer);
            dom.removeEventListener("canplaythrough", success, false);
            dom.removeEventListener("error", failure, false);
            if (__audioSupport.USE_LOADER_EVENT)
                dom.removeEventListener(__audioSupport.USE_LOADER_EVENT, success, false);
        };

        var timer = setTimeout(function () {
            if (dom.readyState === 0)
                failure();
            else
                success();
        }, 8000);

        var success = function () {
            clearEvent();
            onComplete && onComplete(null, dom);
        };

        var failure = function () {
            clearEvent();
            var message = 'load audio failure - ' + url;
            cc.log(message);
            onComplete && onComplete(new Error(message));
        };

        dom.addEventListener("canplaythrough", success, false);
        dom.addEventListener("error", failure, false);
        if (__audioSupport.USE_LOADER_EVENT)
            dom.addEventListener(__audioSupport.USE_LOADER_EVENT, success, false);
        return dom;
    }


    var arrayBufferHandler = (url, options, callback, img) => {
        var data = window.resMap[url];

        var img = new Image();
        function loadCallback() {
            img.removeEventListener('load', loadCallback);
            img.removeEventListener('error', errorCallback);

            callback(null, img);
        }
        function errorCallback() {
            img.removeEventListener('load', loadCallback);
            img.removeEventListener('error', errorCallback);

            // Retry without crossOrigin mark if crossOrigin loading fails
            // Do not retry if protocol is https, even if the image is loaded, cross origin image isn't renderable.

            callback(new Error('Load image (' + url + ') failed'));
        }

        img.addEventListener('load', loadCallback);
        img.addEventListener('error', errorCallback);
        img.src = data;
    };




    var downloadText = function (url, options, onComplete) {
        let data = window.resMap[url];
        onComplete(null, data);
    };


    let downloadArrayBuffer = function (url, options, onComplete) {
        let str = window.resMap[url];
        let data = base64toArray(str);
        onComplete(null, data);
    };


    function loadWebAudio(url, options, onComplete) {
        if (!context)
            callback(new Error(debug.getError(4926)));

        var data = window.resMap[url];
        data = base64toArray(data);

        if (data) {
            context["decodeAudioData"](data.buffer, function (buffer) {
                //success
                onComplete(null, buffer);
            }, function (e) {
                //error
                onComplete(e, null);
            });
        } else {
            onComplete(null, file);
        }
    }


    var downloadAudio = function (url, options, onComplete) {
        if (options.audioLoadMode !== cc.AudioClip.LoadMode.DOM_AUDIO) {
            loadWebAudio(url, options, onComplete);
        }
        else {
            downloadDomAudio(url, options, onComplete);
        }
    };

    var downloadAudio = (!CC_EDITOR || !Editor.isMainProcess) ? (formatSupport.length === 0 ? unsupported : (__audioSupport.WEB_AUDIO ? downloadAudio : downloadDomAudio)) : null;


    function downloadScript(url, options, onComplete) {
        var d = document, s = document.createElement('script');

        // s.type = "text/javascript";
        s.charset = "utf-8";
        s.text = window.resMap[url];

        d.body.appendChild(s);

        onComplete(null);
    }


    var downloadJson = function (url, options, onComplete) {
        let data = window.resMap[url];
        data = JSON.parse(data);
        onComplete(null, data);
    };

    var dwnloadBundleHandler = function (nameOrUrl, options, onComplete) {
        let bundleName = cc.path.basename(nameOrUrl);
        let url = nameOrUrl;
        if (!REGEX.test(url)) url = 'assets/' + bundleName;
        var version = options.version || cc.assetManager.downloader.bundleVers[bundleName];
        var count = 0;
        var config = `${url}/config.${version ? version + '.' : ''}json`;
        let out = null, error = null;

        downloadJson(config, options, function (err, response) {
            if (err) {
                error = err;
            }
            out = response;
            out && (out.base = url + '/');

            count++;
            if (count === 2) {
                onComplete(error, out);
            }
        });

        var js = `${url}/index.${version ? version + '.' : ''}js`;
        downloadScript(js, options, function (err) {
            if (err) {
                error = err;
            }
            count++;
            if (count === 2) {
                onComplete(error, out);
            }
        });
    };



    // support font
    var _canvasContext = null;
    var _testString = "BES bswy:->@123\u4E01\u3041\u1101";

    var _fontFaces = Object.create(null);
    var _intervalId = -1;
    var _loadingFonts = [];
    var _timeout = 1000;

    
    function _checkFontLoaded() {
        let allFontsLoaded = true;
        let now = Date.now();

        for (let i = _loadingFonts.length - 1; i >= 0; i--) {
            let fontLoadHandle = _loadingFonts[i];
            let fontFamily = fontLoadHandle.fontFamilyName;
            // load timeout
            if (now - fontLoadHandle.startTime > _timeout) {
                cc.warnID(4933, fontFamily);
                fontLoadHandle.onComplete(null, fontFamily);
                _loadingFonts.splice(i, 1);
                continue;
            }

            let oldWidth = fontLoadHandle.refWidth;
            let fontDesc = '40px ' + fontFamily;
            _canvasContext.font = fontDesc;
            let newWidth = cc.textUtils.safeMeasureText(_canvasContext, _testString, fontDesc);
            // loaded successfully
            if (oldWidth !== newWidth) {
                _loadingFonts.splice(i, 1);
                fontLoadHandle.onComplete(null, fontFamily);
            }
            else {
                allFontsLoaded = false;
            }
        }

        if (allFontsLoaded) {
            clearInterval(_intervalId);
            _intervalId = -1;
        }
    }

    var downloadFont = (url, options, onComplete) => {

        var ttfIndex = url.lastIndexOf(".ttf");

        var slashPos = url.lastIndexOf("/");
        var fontFamilyName;
        if (slashPos === -1) {
            fontFamilyName = url.substring(0, ttfIndex) + "_LABEL";
        } else {
            fontFamilyName = url.substring(slashPos + 1, ttfIndex) + "_LABEL";
        }
        if (fontFamilyName.indexOf(' ') !== -1) {
            fontFamilyName = '"' + fontFamilyName + '"';
        }

        // Already loaded fonts
        if (_fontFaces[fontFamilyName]) {
            return onComplete(null, fontFamilyName);
        }

        if (!_canvasContext) {
            let labelCanvas = document.createElement('canvas');
            labelCanvas.width = 100;
            labelCanvas.height = 100;
            _canvasContext = labelCanvas.getContext('2d');
        }

        // Default width reference to test whether new font is loaded correctly
        let fontDesc = '40px ' + fontFamilyName;
        _canvasContext.font = fontDesc;
        let refWidth = cc.textUtils.safeMeasureText(_canvasContext, _testString, fontDesc);

        // Setup font face style
        let fontStyle = document.createElement("style");
        fontStyle.type = "text/css";
        let fontStr = "";
        if (isNaN(fontFamilyName - 0))
            fontStr += "@font-face { font-family:" + fontFamilyName + "; src:";
        else
            fontStr += "@font-face { font-family:'" + fontFamilyName + "'; src:";

        var data = "url(data:application/x-font-woff;charset=utf-8;base64,PASTE-BASE64-HERE) format(\"woff\");";
        data = data.replace("PASTE-BASE64-HERE", window.resMap[url]);

        fontStr += data;
        fontStyle.textContent = fontStr + "}";
        document.body.appendChild(fontStyle);

        // Preload font with div
        let preloadDiv = document.createElement("div");
        let divStyle = preloadDiv.style;
        divStyle.fontFamily = fontFamilyName;
        preloadDiv.innerHTML = ".";
        divStyle.position = "absolute";
        divStyle.left = "-100px";
        divStyle.top = "-100px";
        document.body.appendChild(preloadDiv);

        //onComplete(null, fontFamilyName);

        //Save loading font
        let fontLoadHandle = {
            fontFamilyName,
            refWidth,
            onComplete,
            startTime: Date.now()
        }

        _loadingFonts.push(fontLoadHandle);
        if (_intervalId === -1) {
            _intervalId = setInterval(_checkFontLoaded, 100);
        }
    }

    var downloadVideo = (url, options, onComplete) => {
        onComplete(null);
    }


    // 添加加载函数
    cc.assetManager.downloader.register('bundle', dwnloadBundleHandler);

    cc.assetManager.downloader.register('.png', arrayBufferHandler);
    cc.assetManager.downloader.register('.jpg', arrayBufferHandler);
    cc.assetManager.downloader.register('.jpeg', arrayBufferHandler);



    cc.assetManager.downloader.register('.json', downloadJson);

    cc.assetManager.downloader.register('.plist', downloadText);

    cc.assetManager.downloader.register('.bin', downloadArrayBuffer);

    cc.assetManager.downloader.register('.mp3', downloadAudio);

    cc.assetManager.downloader.register('.ttf', downloadFont);

    cc.assetManager.downloader.register('.mp4', downloadVideo);


    var splash = document.getElementById('splash');
    splash.style.display = 'block';
}

window.boot();
