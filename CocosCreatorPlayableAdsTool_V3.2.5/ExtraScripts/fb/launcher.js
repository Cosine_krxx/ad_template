    var zlibAscii85 = "{#html}";
    var str = decode_ascii85(zlibAscii85);
    var data = new Array(str.length);
    for (i = 0, il = str.length; i < il; ++i)  data[i] = str.charCodeAt(i);
    var inflate = new Zlib.Inflate(data);
    var decompress = inflate.decompress();
    var plain = new TextDecoder("utf-8").decode(decompress);
    document.write(plain);