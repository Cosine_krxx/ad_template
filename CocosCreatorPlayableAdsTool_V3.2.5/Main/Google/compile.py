#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import sys
import zipfile

if sys.getdefaultencoding() != 'utf-8':
  reload(sys)
  sys.setdefaultencoding('utf-8')

RootDir = sys.argv[1]
sys.path.append(RootDir+"/Main/Default")
import Utility

print("Export Google ...")


cocosInlinePath = RootDir + '/Export/Default/index_NoCompress.html'

headScrPath = RootDir + '/Main/Google/head.txt'
adScrPath = RootDir + '/Main/Google/google.js'

# exportRoot = RootDir + '/Export/Google'
exportPath = RootDir + '/Export/Google/index.html'
exportZipPath = RootDir + '/Export/Google/index.zip'



def compressToZip():
  with zipfile.ZipFile(exportZipPath, 'w', zipfile.ZIP_DEFLATED) as zf:
    zf.write(exportPath, 'index.html')
    zf.close()

  os.remove(exportPath)
    


def start():
  root = os.path.dirname(exportPath)
  Utility.createExportRoot(root)

  htmlInlinedStr = Utility.getPlatformHtmlStr(cocosInlinePath, adScrPath, headScrPath)
  Utility.writeToPath(exportPath, htmlInlinedStr)

  compressToZip()

if __name__ == '__main__':
  start()