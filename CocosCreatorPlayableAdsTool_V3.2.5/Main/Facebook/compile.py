#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import sys
import zipfile
# import zlib

if sys.getdefaultencoding() != 'utf-8':
  reload(sys)
  sys.setdefaultencoding('utf-8')

RootDir = sys.argv[1]
sys.path.append(RootDir+"/Main/Default")
import Utility

print("Export Facebook ...")
# print("BuildDir:",BuildDir)

cocosInlinePath = RootDir + '/Export/Default/index_Boot.html'
cocosUncompressedPath = RootDir + '/Export/Default/index_NoCompress.html'
adScrPath = RootDir + '/Main/Facebook/facebook.js'

htmlNoAssetsPath = RootDir + '/Export/Default/Depart/index-template.html'#沒有素材和cocos引擎的html

fbHtmlTemplatePath = RootDir + '/Export/Default/index-noAssets.html'#加入Fb規範的html（沒有素材和cocos引擎）
assetsJsPath = RootDir + '/Export/Default/Depart/assets.js'#含素材和cocos引擎 的js

exportPath = RootDir + '/Export/Facebook/index.html'
exportPath_Speed = RootDir + '/Export/Facebook/uncompression/index.html'
exportZipPath_Speed = RootDir + '/Export/Facebook/uncompression/index.zip'

fbHtmlCompressionPath = RootDir + '/Export/Default/index-compression-template-fb.html'
fbJsPath = RootDir + '/Export/Default/launcher-fb.js'
exportZipPath = RootDir + '/Export/Facebook/index.zip'

def createExportRoots():
  root = os.path.dirname(exportPath)
  Utility.createExportRoot(root)

  uncompressRoot = os.path.dirname(exportPath_Speed)
  Utility.createExportRoot(uncompressRoot)

def exportPlatformFile(sourcePath, exportPath):  
  htmlInlinedStr = Utility.getPlatformHtmlStr(sourcePath, adScrPath)
  Utility.writeToPath(exportPath, htmlInlinedStr)

def compressToZip():
  htmlInlinedStr = Utility.getPlatformHtmlStr(fbHtmlCompressionPath, adScrPath)
  Utility.writeToPath(fbHtmlCompressionPath, htmlInlinedStr)

  with zipfile.ZipFile(exportZipPath, 'w', zipfile.ZIP_DEFLATED) as zf:
    zf.write(fbHtmlCompressionPath, 'index.html')
    zf.write(fbJsPath, 'launcher.js')
    zf.close()

#沒壓縮的zip
def zipFBAsset():
  htmlInlinedStr = Utility.getPlatformHtmlStr(htmlNoAssetsPath, adScrPath)
  Utility.writeToPath(fbHtmlTemplatePath, htmlInlinedStr)

  with zipfile.ZipFile(exportZipPath_Speed, 'w', zipfile.ZIP_DEFLATED) as zf:
    zf.write(fbHtmlTemplatePath, 'index.html')
    zf.write(assetsJsPath, 'assets.js')
    zf.close()
  os.remove(fbHtmlTemplatePath) 

def start():
  createExportRoots()

  exportPlatformFile(cocosInlinePath, exportPath)
  exportPlatformFile(cocosUncompressedPath, exportPath_Speed)

  compressToZip()
  zipFBAsset()
  

if __name__ == '__main__':
  start()