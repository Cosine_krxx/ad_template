
if (window.mraid === undefined) {
	window.boot();
}
else {
	if (mraid.getState() === 'loading') {
		mraid.addEventListener('ready', onSdkReady);
	} else {
		onSdkReady();
	}
}

function onSdkReady() {
	if (mraid.isViewable()) {
		showMyAd();
	} else {
		mraid.addEventListener('viewableChange', function (viewable) {
			if (viewable) {
				mraid.removeEventListener('viewableChange', arguments.callee);
				showMyAd();
			}
		});
	}
}

function showMyAd() {
	mraid.getMaxSize();
	window.boot();
}

function userClickedDownloadButton() {
	let url = NEND_API.getClickThroughUrl();
	console.log("Target Url : " + url);

	if (window.mraid === undefined) {
		window.open(url);
	} else {
		mraid.open(url);
	}
}