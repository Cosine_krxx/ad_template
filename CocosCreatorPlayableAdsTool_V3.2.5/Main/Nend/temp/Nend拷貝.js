function loadScript( url, callback ) {
  var script = document.createElement( "script" )
  script.type = "text/javascript";
  if(script.readyState) {  // only required for IE <9
    script.onreadystatechange = function() {
      if ( script.readyState === "loaded" || script.readyState === "complete" ) {
        script.onreadystatechange = null;
        callback();
      }
    };
  } else {  //Others
    script.onload = function() {
      callback();
    };
  }

  script.src = url;
  document.getElementsByTagName( "head" )[0].appendChild( script );
}

    if (mraid.getState() === 'loading') {
  mraid.addEventListener('ready', onSdkReady);
} else {
  onSdkReady();
}

function onSdkReady() {
  if (mraid.isViewable()) {
    showMyAd();
  } else {
    mraid.addEventListener('viewableChange', function (viewable) {
      if (viewable) {
        mraid.removeEventListener('viewableChange', arguments.callee);
        showMyAd();
      }
    });
  }
}

function showMyAd() {
  mraid.getMaxSize();
  window.boot();

  
  loadScript("https://static-interactive-api.nend.net/nend_interactive.js", function() {
    NEND_API.callAtStartOfAd();
    console.log("-> start");
  });
}

function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

  if (/windows phone/i.test(userAgent)) {
    return "Windows Phone";
  }

  if (/android/i.test(userAgent)) {
    return "Android";
  }

  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    return "iOS";
  }
  return "unknown";
}

function userClickedDownloadButton() {
  let url = NEND_API.getClickThroughUrl();
  console.log("Target Url : " + url);

  mraid.open(url);

  NEND_API.callAtInteraction('Go_To_Store');
  console.log("-> go to store");
  NEND_API.callAtEndOfAd();
  console.log("-> end");
}