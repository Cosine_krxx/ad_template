#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import sys
import zipfile

if sys.getdefaultencoding() != 'utf-8':
  reload(sys)
  sys.setdefaultencoding('utf-8')

RootDir = sys.argv[1]
sys.path.append(RootDir+"/Main/Default")
import Utility
print("Export Maio(i-mobile) ...")

maioSDK_Name = 'maio.1.1.6.min'
maioTestTool_Name = 'maio.nativesdk-test-tool.1.1.6'

cocosInlinePath = RootDir + '/Export/Default/index_NoBoot_NoCompress.html'

adScrPath = RootDir + '/Main/Maio/maio.js'
headScrPath = RootDir + '/Main/Maio/head.txt'

maioSDKPath = RootDir + '/Main/Maio/' + maioSDK_Name + '.js'
maioTestToolPath = RootDir + '/Main/Maio/' + maioTestTool_Name + '.js'

exportPath = RootDir + '/Export/Maio(i-mobile)/index.html'
exportZipPath = RootDir + '/Export/Maio(i-mobile)/index.zip'

def createExportRoot():
  root = os.path.dirname(exportPath)
  Utility.createExportRoot(root)


def compressToZip():
  with zipfile.ZipFile(exportZipPath, 'w', zipfile.ZIP_DEFLATED) as zf:
    zf.write(exportPath, 'index.html')
    zf.write(maioSDKPath, maioSDK_Name + '.js')
    zf.write(maioTestToolPath, maioTestTool_Name + '.js')
    zf.close()

  os.remove(exportPath)

def start():
  createExportRoot()

  htmlInlinedStr = Utility.getPlatformHtmlStr(cocosInlinePath, adScrPath, headScrPath)
  Utility.writeToPath(exportPath, htmlInlinedStr)
  
  compressToZip()

if __name__ == '__main__':
  start()