
if (window.mraid === undefined) {
	window.boot();
}
else {
	if (mraid.getState() === 'loading') {
		mraid.addEventListener('ready', onSdkReady);
	} else {
		onSdkReady();
	}
}

function onSdkReady() {
	if (mraid.isViewable()) {
		showMyAd();
	} else {
		mraid.addEventListener('viewableChange', function (viewable) {
			if (viewable) {
				mraid.removeEventListener('viewableChange', arguments.callee);
				showMyAd();
			}
		});
	}
}

function showMyAd() {
	mraid.getMaxSize();
	window.boot();
}

function userClickedDownloadButton() {
	var tracker = gsm.api.core.getTracker();

	if (tracker == undefined) {
		window.open("https://www.g-kit.co.jp/playablelp");
	} else {
		console.log("redirectByRdbOnly")
		tracker.redirectByRdbOnly('Click', false);
	}
}