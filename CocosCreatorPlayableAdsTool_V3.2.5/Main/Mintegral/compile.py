#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import sys
import zipfile
import shutil

if sys.getdefaultencoding() != 'utf-8':
  reload(sys)
  sys.setdefaultencoding('utf-8')

RootDir = sys.argv[1]
sys.path.append(RootDir+"/Main/Default")
import Utility
print("Export Mintegral ...")

cocosInlinePath = RootDir + '/Export/Default/index_NoBoot_NoCompress.html'

headScrPath = RootDir + '/Main/Mintegral/head.txt'#head alread exist in template
adScrPath = RootDir + '/Main/Mintegral/mintegral.js'

exportPath = RootDir + '/Export/Mintegral/index/index.html'
exportZipPath = RootDir + '/Export/Mintegral/index.zip'


def createExportRoots():
  root = os.path.dirname(exportZipPath)
  Utility.createExportRoot(root)

  dirRoot = os.path.dirname(exportPath)
  Utility.createExportRoot(dirRoot)

def compressToZip():
  with zipfile.ZipFile(exportZipPath, 'w', zipfile.ZIP_DEFLATED) as zf:
    zf.write(exportPath, 'index/index.html')
    zf.close()

  dirRoot = os.path.dirname(exportPath)
  shutil.rmtree(dirRoot)


def start():
  createExportRoots()
  
  htmlInlinedStr = Utility.getPlatformHtmlStr(cocosInlinePath, adScrPath)#head alread exist in template
  Utility.writeToPath(exportPath, htmlInlinedStr)

  compressToZip()

if __name__ == '__main__':
  start()