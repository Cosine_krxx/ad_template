if (window.gameReady && window.gameReady()) {
}

if (window.gameReady === undefined) {
	window.boot();
}

function gameStart() {
	window.boot();
}

function gameClose() {
	window.gameEnd && window.gameEnd();
}

function userClickedDownloadButton() {
	window.install && window.install();
	window.gameEnd && window.gameEnd();
}