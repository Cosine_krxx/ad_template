
function getScript(e,n){var i=document.createElement("script");i.type="text/javascript",i.async=!0,n&&(i.onload=n),i.src=e,document.head.appendChild(i)}function parseMessage(e){var n=e.data,i=n.indexOf(DOLLAR_PREFIX+RECEIVE_MSG_PREFIX);return-1!==i?getMessageParams(n.slice(i+2)):{}}function getMessageParams(e){var n,i=[],t=e.split("/"),a=t.length;if(-1===e.indexOf(RECEIVE_MSG_PREFIX)){if(a>=2&&a%2==0)for(n=0;a>n;n+=2)i[t[n]]=t.length<n+1?null:decodeURIComponent(t[n+1])}else{var o=e.split(RECEIVE_MSG_PREFIX);void 0!==o[1]&&(i=JSON&&JSON.parse(o[1]))}return i}function getDapi(e){var n=parseMessage(e);(n.hasOwnProperty("name")||window.boot(),n&&n.name!==GET_DAPI_URL_MSG_NAME)||getScript(n.data,onDapiReceived)}function invokeDapiListeners(){for(var e in dapiEventsPool)dapiEventsPool.hasOwnProperty(e)&&dapi.addEventListener(e,dapiEventsPool[e])}function onDapiReceived(){console.log("-> receive !"),dapi=window.dapi,window.removeEventListener("message",getDapi),invokeDapiListeners()}function init(){window.dapi.isDemoDapi&&(window.parent.postMessage(DOLLAR_PREFIX+SEND_MSG_PREFIX+JSON.stringify({state:"getDapiUrl"}),"*"),window.addEventListener("message",getDapi,!1))}var DOLLAR_PREFIX="$$",RECEIVE_MSG_PREFIX="DAPI_SERVICE:",SEND_MSG_PREFIX="DAPI_AD:",GET_DAPI_URL_MSG_NAME="connection.getDapiUrl",dapiEventsPool={},dapi=window.dapi||{isReady:function(){return!1},addEventListener:function(e,n){dapiEventsPool[e]=n},removeEventListener:function(e){delete dapiEventsPool[e]},isDemoDapi:!0};init();

window.onload = function () {
	(dapi.isReady()) ? onReadyCallback() : dapi.addEventListener("ready", onReadyCallback);
	//here you can put other code that not related to dapi logic
};

function onReadyCallback() {
	//no need to listen to this event anymore
	dapi.removeEventListener("ready", onReadyCallback);

	if (dapi.isViewable()) {
		adVisibleCallback({ isViewable: true });
	}

	dapi.addEventListener("viewableChange", adVisibleCallback);
	dapi.addEventListener("adResized", adResizeCallback);
	dapi.addEventListener("audioVolumeChange", audioChangeCallback);
}

function adVisibleCallback(event) {
	console.log("isViewable " + event.isViewable);
	if (event.isViewable) {
		screenSize = dapi.getScreenSize();
		audioVolume = dapi.getAudioVolume();
		window.boot();
	} else {
		//PAUSE the ad and MUTE sounds
	}
}

function audioChangeCallback(event) {
	audioVolume = event;
}

function adResizeCallback(event) {
	screenSize = event;
	console.log("ad was resized width " + event.width + " height " + event.height);
}

function userClickedDownloadButton() {
	dapi.openStoreUrl();
}