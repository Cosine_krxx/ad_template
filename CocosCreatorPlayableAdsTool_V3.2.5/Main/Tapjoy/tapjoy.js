if (window.TJ_API) {
  var AdAPI = {
    skipAd: function () {
      userClickedDownloadButton();
    }
  };

  window.TJ_API.setPlayableAPI(AdAPI);
}

function userClickedDownloadButton() {
  window.TJ_API && window.TJ_API.click();
}