#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import sys

if sys.getdefaultencoding() != 'utf-8':
  reload(sys)
  sys.setdefaultencoding('utf-8')

RootDir = sys.argv[1]
sys.path.append(RootDir+"/Main/Default")
import Utility
print("Export Tapjoy ...")

cocosInlinePath = RootDir + '/Export/Default/index_Boot.html'
cocosUncompressedPath = RootDir + '/Export/Default/index_NoCompress.html'

adScrPath = RootDir + '/Main/Tapjoy/tapjoy.js'

exportPath = RootDir + '/Export/Tapjoy/index.html'
exportPath_Speed = RootDir + '/Export/Tapjoy/uncompression/index.html'



def createExportRoots():
  root = os.path.dirname(exportPath)
  Utility.createExportRoot(root)

  uncompressRoot = os.path.dirname(exportPath_Speed)
  Utility.createExportRoot(uncompressRoot)


def exportPlatformFile(sourcePath, exportPath):
  htmlInlinedStr = Utility.getPlatformHtmlStr(sourcePath, adScrPath)
  Utility.writeToPath(exportPath, htmlInlinedStr)

def start():
  createExportRoots()

  exportPlatformFile(cocosInlinePath, exportPath)
  exportPlatformFile(cocosUncompressedPath, exportPath_Speed)

if __name__ == '__main__':
  start()