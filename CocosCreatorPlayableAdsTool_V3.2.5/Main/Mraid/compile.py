#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import json
import os
import sys
import base64
import zlib

if sys.getdefaultencoding() != 'utf-8':
  reload(sys)
  sys.setdefaultencoding('utf-8')

RootDir = sys.argv[1]
sys.path.append(RootDir+"/Main/Default")
import Utility
print("Export Mraid ...")


cocosInlinePath = RootDir + '/Export/Default/index_NoBoot.html'
cocosInline_NoCompressPath = RootDir + '/Export/Default/index_NoCompress.html'

htmlMraidPath = RootDir + '/ExtraScripts/index-mraid-template.html'
compressionDataPath = RootDir + '/Main/Mraid/compression-data.js'


adScrPath = RootDir + '/Main/Mraid/mraid.js'
urlDefinitionPath = RootDir + '/ExtraScripts/url-definition.json'



exportPath = RootDir + '/Export/Mraid/tag.txt'
exportJSPath = RootDir + '/Export/Mraid/mraid-ad.js'
exportIndexPath = RootDir + '/Export/Mraid/index.html'



zlibAscii85MatchKey = '{#html}'



def createExportRoots():
  root = os.path.dirname(exportPath)
  Utility.createExportRoot(root)


def getZlibAscii85(text):
  compress = zlib.compress(text.encode(), 9)
  a85raw = base64.a85encode(compress).decode()
  a85new = '<~' + a85raw.replace('\n', '') + '~>'
  a85new = a85new.replace('\\', '\\\\').replace('"', '\\"').replace('<', '{')
  return a85new


def start():
  createExportRoots()

  #export html
  htmlInlinedStr = Utility.getPlatformHtmlStr(cocosInlinePath, adScrPath)
  Utility.writeToPath(exportIndexPath, htmlInlinedStr)
  


  #export js
  htmlInlinedStr = Utility.read_in_chunks(cocosInline_NoCompressPath)
  zlibAscii85 = getZlibAscii85(htmlInlinedStr)

  adStr = Utility.getAdScript(adScrPath)
  compressionDataStr = Utility.read_in_chunks(compressionDataPath)
  compressionDataStr = compressionDataStr.replace(zlibAscii85MatchKey, zlibAscii85)
  compressionDataStr = compressionDataStr.replace(Utility.headMatchKey, "")
  compressionDataStr = compressionDataStr.replace(Utility.bodyMatchKey, "")
  compressionDataStr = compressionDataStr.replace(Utility.adMatchKey, adStr, 1)

  Utility.writeToPath(exportJSPath, compressionDataStr)

  #輸出tag.txt用在ironSource的測試工具
  urlDefObj = json.loads(Utility.read_in_chunks(urlDefinitionPath))

  mraidStr = Utility.read_in_chunks(htmlMraidPath)
  mraidStr = mraidStr.replace('[INPUT DATA URL]', urlDefObj['Mraid']['[INPUT DATA URL]'])
  Utility.writeToPath(exportPath, mraidStr)



if __name__ == '__main__':
  start()