if (window.mraid === undefined) {
	window.boot();
}
else {
	if (mraid.getState() === 'loading') {
		mraid.addEventListener('ready', onSdkReady);
	} else {
		onSdkReady();
	}
}

function onSdkReady() {
	if (mraid.isViewable()) {
		showMyAd();
	} else {
		mraid.addEventListener('viewableChange', function (viewable) {
			if (viewable) {
				mraid.removeEventListener('viewableChange', arguments.callee);
				showMyAd();
			}
		});
	}
}

function showMyAd() {
	mraid.getMaxSize();
	window.boot();
}

function getMobileOperatingSystem() {
	var userAgent = navigator.userAgent || navigator.vendor || window.opera;

	if (/windows phone/i.test(userAgent)) {
		return "Windows Phone";
	}

	if (/android/i.test(userAgent)) {
		return "Android";
	}

	if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
		return "iOS";
	}
	return "unknown";
}


function userClickedDownloadButton() {

	let googlePlayStoreUrl = "[INPUT GOOGLE PLAY STORE URL]";
	let appStoreUrl = "[INPUT APP STORE URL]";

	console.log("Both Store Url : " + googlePlayStoreUrl + " " + appStoreUrl);

	let url = appStoreUrl;
	if (getMobileOperatingSystem() === "Android")
		url = googlePlayStoreUrl;

	console.log("Target Url : " + url);

	if (window.mraid === undefined) {
		window.open(url)
	} else {
		mraid.open(url);
	}
}