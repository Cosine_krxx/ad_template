#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import sys

if sys.getdefaultencoding() != 'utf-8':
  reload(sys)
  sys.setdefaultencoding('utf-8')

RootDir = sys.argv[1]
sys.path.append(RootDir+"/Main/Default")
import Utility
print("Export Applovin ...")

cocosInlinePath = RootDir + '/Export/Default/index_NoBoot.html'
cocosUncompressedPath = RootDir + '/Export/Default/index_NoBoot_NoCompress.html'
#cocosInlinePath = RootDir + '/Export/Default/index_Mraid.html'

adScrPath = RootDir + '/Main/Applovin/applovin.js'

exportPath = RootDir + '/Export/Applovin/index.html'
exportPath_Speed = RootDir + '/Export/Applovin/uncompression/index.html'


def createExportRoots():
  root = os.path.dirname(exportPath)
  Utility.createExportRoot(root)

  uncompressRoot = os.path.dirname(exportPath_Speed)
  Utility.createExportRoot(uncompressRoot)

def exportPlatformFile(sourcePath, exportPath):
  htmlInlinedStr = Utility.getPlatformHtmlStr(sourcePath, adScrPath)
  Utility.writeToPath(exportPath, htmlInlinedStr)

def start():
  createExportRoots()

  exportPlatformFile(cocosInlinePath, exportPath)
  exportPlatformFile(cocosUncompressedPath, exportPath_Speed)

if __name__ == '__main__':
  start()