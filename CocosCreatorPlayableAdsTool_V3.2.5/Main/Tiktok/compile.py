#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import sys
import zipfile

if sys.getdefaultencoding() != 'utf-8':
  reload(sys)
  sys.setdefaultencoding('utf-8')

RootDir = sys.argv[1]
sys.path.append(RootDir+"/Main/Default")
import Utility
print("Export Tiktok ...")

cocosInlinePath = RootDir + '/Export/Default/index_NoCompress.html'

bodyScrPath = RootDir + '/Main/Tiktok/body.txt'
adScrPath = RootDir + '/Main/Tiktok/tiktok.js'

adConfigPath = RootDir + '/Main/Tiktok/config.json'

exportPath = RootDir + '/Export/Tiktok/index.html'
exportZipPath = RootDir + '/Export/Tiktok/index.zip'


def createExportRoots():
  root = os.path.dirname(exportPath)
  Utility.createExportRoot(root)

def compressToZip():
  with zipfile.ZipFile(exportZipPath, 'w', zipfile.ZIP_DEFLATED) as zf:
    zf.write(exportPath, 'index.html')
    zf.write(adConfigPath, 'config.json')
    zf.close()
  
  os.remove(exportPath)


def start():
  createExportRoots()

  htmlInlinedStr = Utility.getPlatformHtmlStr(cocosInlinePath, adScrPath, "", bodyScrPath)
  Utility.writeToPath(exportPath, htmlInlinedStr)

  compressToZip()

if __name__ == '__main__':
  start()