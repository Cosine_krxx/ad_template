import os
import base64
import json

headMatchKey = "{#head}"
adMatchKey = '{#ad}'
bodyMatchKey = '{#body}'

fileByteList = ['.png', '.jpg', '.gif', '.mp3', '.ttf', '.bin', '.mp4']

base64PreList = {
  '.png' : 'data:image/png;base64,',
  '.jpg' : 'data:image/jpeg;base64,',
  '.gif' : 'data:image/gif;base64,',
  '.mp3' : '',
  '.ttf' : '',
  '.bin' : '', 
  '.mp4' : 'data:video/mp4;base64,'
}

def read_in_chunks(filePath, chunk_size=1024*1024):
  """
  Lazy function (generator) to read a file piece by piece.
  Default chunk size: 1M
  You can set your own chunk size
  """
  extName = os.path.splitext(filePath)[1]
  if extName in fileByteList:
    file_object = open(filePath, 'rb')
    base64Str = base64.b64encode(file_object.read())
    preName = base64PreList[extName]
    if preName != None:
      base64Str = preName + base64Str.decode()
    return base64Str
  elif extName == '':
    return None
  
  file_object = open(filePath, encoding="utf-8")
  return file_object.read()

def writeToPath(path, data):
  with open(path,'w', encoding="utf-8") as f: # 如果filename不存在会自动创建， 'w'表示写数据，写之前会清空文件中的原有数据！
    f.write(data)

def createExportRoot(path):
  if not os.path.exists(path):
    os.mkdir(path)

def getAdScript(adScrPath):
  file_object = open(adScrPath, 'rb')
  scriptStr = file_object.read().decode()

  if '[INPUT GOOGLE PLAY STORE URL]' in scriptStr:
    platformName = os.path.basename(os.path.dirname(adScrPath))
    # print("platformName:", platformName)

    RootDir = os.path.dirname(os.path.dirname(adScrPath))+"/.."
    urlDefinitionPath = RootDir + '/ExtraScripts/url-definition.json'
    urlDefObj = json.loads(read_in_chunks(urlDefinitionPath))

    scriptStr = scriptStr.replace('[INPUT GOOGLE PLAY STORE URL]', urlDefObj[platformName]['[INPUT GOOGLE PLAY STORE URL]'])
    scriptStr = scriptStr.replace('[INPUT APP STORE URL]', urlDefObj[platformName]['[INPUT APP STORE URL]'])

  return scriptStr

# 在inlinedHtmlPath中加入平台規範後回傳
def getPlatformHtmlStr(inlinedHtmlPath, adScrPath, headScrPath = "", bodyScrPath = ""):
  
  htmlStr = read_in_chunks(inlinedHtmlPath)
  htmlStr = htmlStr.replace("<!-- Ad Plugin -->", "")
  htmlStr = htmlStr.replace("<!-- Ad Plugin End -->", "")

  adStr = getAdScript(adScrPath)

  headStr = ""
  if headScrPath:
    headStr = read_in_chunks(headScrPath)

  bodyStr = ""
  if bodyScrPath:
    bodyStr = read_in_chunks(bodyScrPath)

  htmlStr = htmlStr.replace(adMatchKey, adStr, 1)
  htmlStr = htmlStr.replace(headMatchKey, headStr)
  htmlStr = htmlStr.replace(bodyMatchKey, bodyStr)
  
  return  htmlStr