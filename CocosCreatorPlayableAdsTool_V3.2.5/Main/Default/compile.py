#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from xml.dom.minidom import parse
import xml.dom.minidom
import json
import os
import time
import sys
import codecs
import cgi
# import HTMLParser
import re
import base64
import zipfile
import zlib

import Utility

if sys.getdefaultencoding() != 'utf-8':
  reload(sys)
  sys.setdefaultencoding('utf-8')

RootDir = sys.argv[1]
BuildDir = os.path.abspath(os.path.join(RootDir, os.pardir))
print("Default:",RootDir)
print(BuildDir)



htmlPath = RootDir + '/ExtraScripts/index-template.html'
portraitPath = RootDir + '/ExtraScripts/orientation/portrait.html'
portraitJSPath = RootDir + '/ExtraScripts/orientation/portrait_PC_fix_min.js'
landscapePath = RootDir + '/ExtraScripts/orientation/landscape.html'

htmlCompressionPath = RootDir + '/ExtraScripts/index-compression-template.html'
mainScrPath = RootDir + '/ExtraScripts/#js/main_boot_min.js'
mainNoBootScrPath = RootDir + '/ExtraScripts/#js/main_no_boot_min.js'

splashScrPath = RootDir + '/Splash/splash.png'
exportRoot = RootDir + '/Export/Default'

exportPath = RootDir + '/Export/Default/index.html'
exportNoCompressPath = RootDir + '/Export/Default/index_NoCompress.html'
exportNoCompressNoBootPath = RootDir + '/Export/Default/index_NoBoot_NoCompress.html'

#分開html和引擎, 素材資源
exportDepartRoot = RootDir + '/Export/Default/Depart'
assetsJsPath = RootDir + '/Export/Default/Depart/assets.js'
exportNoAssetsHtmlPath = RootDir + '/Export/Default/Depart/index-template.html'

exportBootPath = RootDir + '/Export/Default/index_Boot.html'
exportNoBootPath = RootDir + '/Export/Default/index_NoBoot.html'
exportWithMraidTagPath = RootDir + '/Export/Default/index_Mraid.html'

fbHtmlCompressionPath = RootDir + '/ExtraScripts/fb/index-compression-template.html'
fbLauncherJsPath = RootDir + '/ExtraScripts/fb/launcher.js'
exportFbHtmlCompressionPath = RootDir + '/Export/Default/index-compression-template-fb.html'
exportFbLauncherJsPath = RootDir + '/Export/Default/launcher-fb.js'

settingScrPath = BuildDir + '/build/web-mobile/src/settings.js'
engineScrPath = BuildDir + '/build/web-mobile/cocos2d-js-min.js'
physicsScrPath = BuildDir + '/build/web-mobile/physics-min.js' 

resPath = BuildDir + '/build/web-mobile/assets'
assetsPath = BuildDir + '/build/web-mobile/src/assets'# 插件脚本

orientationMatchKey = '{#orientation}'
orientationJSMatchKey = '{#orientationJS}'
settingMatchKey = '{#settings}'
mainMatchKey = '{#main}'
engineMatchKey = '{#cocosEngine}'
physicsMatchKey = '{#physics}' 

resMapMatchKey = '{#resMap}'
assetsScriptKey = '{#assetsScripts}'
splashMatchKey = './splash.png'

dataMatchKey = '{#data}'
zlibAscii85MatchKey = '{#html}'


addScriptPathList = [settingScrPath, mainScrPath, engineScrPath]

def getResMap(jsonObj, path):
  fileList = os.listdir(path)
  for fileName in fileList:
    absPath = path + '/' + fileName
    if (os.path.isdir(absPath)):
      getResMap(jsonObj, absPath)
    elif (os.path.isfile(absPath)):
      dataStr = Utility.read_in_chunks(absPath)
      if dataStr != None:
        absPath = 'assets' + absPath.replace(resPath, '')
        jsonObj[absPath] = dataStr
        print(absPath)

def getResMapScript():
  # createExportRoot()
  jsonObj = {}
  getResMap(jsonObj, resPath)
  resStr = str("window.resMap = ") + json.dumps(jsonObj)
  return resStr

def getAssets(jsonObj, path):
  if not os.path.exists(path):
    return
  fileList = os.listdir(path)
  for fileName in fileList:
    absPath = path + '/' + fileName
    if (os.path.isdir(absPath)):
      getAssets(jsonObj, absPath)
    elif (os.path.isfile(absPath)):
      dataStr = Utility.read_in_chunks(absPath)
      if dataStr != None:
        print(absPath)
        jsonObj['data'] = jsonObj['data'] + dataStr + '\n\n\t\t'

def getAssetsScript():
  jsonObj = {}
  jsonObj['data'] = ''
  getAssets(jsonObj, assetsPath)
  return jsonObj['data']

def getSplashBase64():
  file_object = open(splashScrPath, 'rb')
  base64Str = base64.b64encode(file_object.read())
  preName = 'data:image/png;base64,'
  base64Str = '\"' + preName + base64Str.decode() + '\"'
  return base64Str

def compressTextToZip(targetPath, filePath, text):
  with zipfile.ZipFile(targetPath, 'w', zipfile.ZIP_DEFLATED) as zf:
    zf.writestr(filePath, text)
    zf.close()

def getZlibAscii85(text):
  compress = zlib.compress(text.encode(), 9)
  a85raw = base64.a85encode(compress).decode()
  a85new = '<~' + a85raw.replace('\n', '') + '~>'
  a85new = a85new.replace('\\', '\\\\').replace('"', '\\"').replace('<', '{')
  return a85new

def getPhysicsStr():
  if os.path.exists(physicsScrPath):
    physicsStr = Utility.read_in_chunks(physicsScrPath)
  else:
    physicsStr = ""
  return physicsStr

def removeCustomRange(htmlStr):
  indexCustomStart = htmlStr.find("<!--CustomStart-->")
  indexCustomEnd = htmlStr.find("<!--CustomEnd-->")
  if len(htmlStr) > indexCustomEnd :
    htmlStr = htmlStr[0: indexCustomStart:] + htmlStr[indexCustomEnd::]
  return htmlStr

def addMraidRange(htmlStr):
  rangeStr = '<script src="mraid.js"></script>\n\t<script type="text/javascript" charset="utf-8">\n\t\t{#data}\n\t</script>'
  return htmlStr.replace('<!--CustomEnd-->', rangeStr)

def addDataRange(htmlStr):
  rangeStr = '<script type="text/javascript" charset="utf-8">\n\t\t{#data}\n\t</script>'
  return htmlStr.replace('<!--CustomEnd-->', rangeStr)


def getFinalJsStr(isBoot):

  resStr = getResMapScript()+'\n'
  settingsStr = Utility.read_in_chunks(settingScrPath) +'\n'
  engineStr = Utility.read_in_chunks(engineScrPath)+'\n'
  physicsStr = getPhysicsStr()+'\n'
  assetsPath = getAssetsScript()+'\n'

  if isBoot:
    mainStr = Utility.read_in_chunks(mainScrPath)+'\n'
  else:
    mainStr = Utility.read_in_chunks(mainNoBootScrPath)+'\n'
  
  finalJsStr = resStr + settingsStr + engineStr + physicsStr + assetsPath + mainStr
  return finalJsStr

def getInLinedHTML(isMraid, isBoot, isAddJsStr=True):
  htmlStr = Utility.read_in_chunks(htmlPath)
  htmlStr = removeCustomRange(htmlStr)

  if(isMraid):
    htmlStr = addMraidRange(htmlStr)
  elif(isAddJsStr):
    htmlStr = addDataRange(htmlStr)    

  splashStr = getSplashBase64()
  htmlStr = htmlStr.replace(splashMatchKey, splashStr, 1)
  
  finalJsStr = getFinalJsStr(isBoot)
  bugStrKey = 'return e||new Error(\"Plist Loader: Parse [\"+t.id+\"] failed\")'
  fixStr = 'return e;'

  finalJsStr = finalJsStr.replace(bugStrKey, fixStr, 1)

  if(isAddJsStr):
    htmlStr = htmlStr.replace(dataMatchKey, finalJsStr, 1)
  else:
    htmlStr = htmlStr.replace('<!--CustomEnd-->', '<script src="assets.js" type="text/javascript" charset="utf-8"></script>')
    Utility.writeToPath(assetsJsPath, finalJsStr)

  # set orientation
  orientationStr = getOrientationH5Str()
  htmlStr = htmlStr.replace(orientationMatchKey, orientationStr)

  orientationJS_Str = getOrientationScript()
  htmlStr = htmlStr.replace(orientationJSMatchKey, orientationJS_Str)

  return htmlStr

def removeHeadBodyAdKey(htmlStr):
  htmlStr = htmlStr.replace(Utility.headMatchKey, "")
  htmlStr = htmlStr.replace(Utility.bodyMatchKey, "")

  indexAdStart = htmlStr.find("<!-- Ad Plugin -->")
  indexAdEnd = htmlStr.find("<!-- Ad Plugin End -->")
  if len(htmlStr) > indexAdEnd :
    htmlStr = htmlStr[0: indexAdStart:] + htmlStr[indexAdEnd::]

  htmlStr = htmlStr.replace("<!-- Ad Plugin End -->", "")
  return htmlStr

def compressHTML(htmlStr):
  zlibAscii85 = getZlibAscii85(htmlStr)
  htmlCompressStr = Utility.read_in_chunks(htmlCompressionPath)
  htmlCompressStr = htmlCompressStr.replace(zlibAscii85MatchKey, zlibAscii85)

  return htmlCompressStr

def compressHtmlToFbJs(htmlStr):
  zlibAscii85 = getZlibAscii85(htmlStr)
  htmlCompressStr = Utility.read_in_chunks(fbLauncherJsPath)
  htmlCompressStr = htmlCompressStr.replace(zlibAscii85MatchKey, zlibAscii85)

  return htmlCompressStr

def getGameOrientation():
  settingsStr = Utility.read_in_chunks(settingScrPath)

  gameOrientation = "portrait"
  if settingsStr.find("orientation:\"landscape\"") > -1 :
    gameOrientation = "landscape"

  return gameOrientation


def getOrientationH5Str():
  gameOrientation = getGameOrientation()

  if gameOrientation == "portrait":
    orientationStr = Utility.read_in_chunks(portraitPath)
  else :
    orientationStr = Utility.read_in_chunks(landscapePath)

  return orientationStr

def getOrientationScript():
  gameOrientation = getGameOrientation()

  if gameOrientation == "portrait":
    orientationScript = Utility.read_in_chunks(portraitJSPath)
    orientationScript = '<script type="text/javascript" charset="utf-8">'+orientationScript+'</script>'
  else :
    orientationScript = ""

  return orientationScript

def start():
  Utility.createExportRoot(exportRoot)
  Utility.createExportRoot(exportDepartRoot)

  htmlStr = getInLinedHTML(False, True)

  #未壓縮html
  Utility.writeToPath(exportNoCompressPath, htmlStr)
  htmlStr = removeHeadBodyAdKey(htmlStr)

  #壓縮的html
  htmlCompressStr = compressHTML(htmlStr)
  Utility.writeToPath(exportBootPath, htmlCompressStr)

  #export clear 壓縮的 html
  htmlCompressStr = htmlCompressStr.replace(Utility.adMatchKey, "")
  htmlCompressStr = htmlCompressStr.replace(Utility.headMatchKey, "")
  Utility.writeToPath(exportPath, htmlCompressStr)
  

  #export no boot
  htmlStr = getInLinedHTML(False, False)
  Utility.writeToPath(exportNoCompressNoBootPath, htmlStr)#no boot 未壓縮html
  htmlStr = removeHeadBodyAdKey(htmlStr)

  htmlCompressStr = compressHTML(htmlStr)
  Utility.writeToPath(exportNoBootPath, htmlCompressStr)#no boot 壓縮後的html


  #export launcher fb
  htmlStr = getInLinedHTML(False, True)
  htmlStr = removeHeadBodyAdKey(htmlStr)
  jsCompressStr = compressHtmlToFbJs(htmlStr)
  Utility.writeToPath(exportFbLauncherJsPath, jsCompressStr)
  htmlCompressStr = Utility.read_in_chunks(fbHtmlCompressionPath)
  Utility.writeToPath(exportFbHtmlCompressionPath, htmlCompressStr)

  #Fb zip 
  Utility.writeToPath(exportNoAssetsHtmlPath, getInLinedHTML(False, True, False))

  #export no boot with Mraid
  # htmlStr = getInLinedHTML(True, False)
  # htmlStr = removeHeadBodyAdKey(htmlStr)

  # htmlCompressStr = compressHTML(htmlStr)
  # writeToPath(exportWithMraidTagPath, htmlCompressStr)
  print("\n\n\n\n\n\n")
  print("Game orientation: ", getGameOrientation() )
  print("Export Default end...")
if __name__ == '__main__':
  start()




