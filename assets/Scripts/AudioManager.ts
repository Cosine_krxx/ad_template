
const { ccclass, property } = cc._decorator;

export enum SEName{
    CONFIRM_BTN,
    CONTROL_BTN,
    UNCLICKABLE_SE,
}

@ccclass
export default class AudioManager extends cc.Component {

    @property(cc.AudioSource)
    private bgm: cc.AudioSource = null;

    @property({
        type: [cc.AudioClip]
    }) private SEClips: cc.AudioClip[] = [];

    private static _instance: AudioManager = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.setSingalton();
    }

    start() {

    }

    // update (dt) {}

    public static playSE(name:SEName){
        let clip:cc.AudioClip = this._instance.SEClips[name];
        cc.audioEngine.play(clip, false, 1);
    }

    public static playBGM() {
        if (!AudioManager._instance.bgm.isPlaying)
            AudioManager._instance.bgm.play();
    }

    public static fadeOutBgm(duration: number = 0.1) {
        let bgm = AudioManager._instance.bgm;

        cc.tween(bgm)
            .to(duration, { volume: 0 })
            .start();
    }

    public static replayBgm(isMute: boolean, duration: number = 0.2) {
        let bgm = AudioManager._instance.bgm;

        if (!isMute)
            bgm.play();

        cc.tween(bgm)
            .call(() => bgm.setCurrentTime(0))
            .delay(0.1)
            .to(duration, { volume: 1 })
            .start();
    }

    private setSingalton() {
        if (AudioManager._instance == null) {
            AudioManager._instance = this;
        }
    }

    onDestroy() {
        AudioManager._instance = null;
    }
}
