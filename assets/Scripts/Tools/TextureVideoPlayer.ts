
const { ccclass, property } = cc._decorator;
const DIFFUSE_TEXTURE: string = "diffuseTexture";

@ccclass
export default class TextureVideoPlayer extends cc.Component {

    @property(cc.Asset)
    private videoClips: cc.Asset[] = [];

    @property(cc.Boolean)
    private isAutoPlay: boolean = false;

    @property(cc.Boolean)
    private isPreview: boolean = true;

    @property(cc.Boolean)
    private isAutoLoop: boolean = false;

    @property(cc.Boolean)
    private isAutoMuted: boolean = false;

    @property([cc.Sprite])
    private targetSprites: cc.Sprite[] = [];

    @property([cc.Material])
    private targetMaterials: cc.Material[] = [];

    private video: HTMLVideoElement = null;
    private canvas: HTMLCanvasElement = null;
    private context: CanvasRenderingContext2D = null;
    private texture: cc.RenderTexture = null;
    private counter: number = 0;

    public get isPlaying() { return !this.video.paused; }
    public get isLoop() { return this.isAutoLoop; }
    public get isMuted() { return this.isAutoMuted; }
    public get playbackRate() { return this.video.playbackRate; }

    //#region Video Event
    private evtOnLoadedMeta: Array<Function> = new Array<Function>();
    public get eventOnLoadedMeta(): Array<Function> { return this.evtOnLoadedMeta; }

    private evtOnLoadedData: Array<Function> = new Array<Function>();
    public get eventOnLoadedData(): Array<Function> { return this.evtOnLoadedData; }

    private evtOnPlaying: Array<Function> = new Array<Function>();
    public get eventOnPlaying(): Array<Function> { return this.evtOnPlaying; }

    private evtOnPause: Array<Function> = new Array<Function>();
    public get eventOnPause(): Array<Function> { return this.evtOnPause; }

    private evtOnEnded: Array<Function> = new Array<Function>();
    public get eventOnEnded(): Array<Function> { return this.evtOnEnded; }
    //#endregion

    public load(index: number) {
        if (this.video == null) {
            this.createVideoElement();
        }

        let url = this.videoClips[index].nativeUrl;
        let resMap = window.resMap === undefined ? url : window.resMap[url];

        this.video.autoplay = true;
        this.video.preload = "auto";
        this.video.loop = this.isAutoLoop;
        this.video.muted = this.isAutoMuted;
        this.video.src = resMap;
    }

    public release() {
        this.releaseTexture();
        this.releaseCanvasElement();
        this.releaseVideoElement();
    }

    public play() {
        this.video.play();
    }

    public pause() {
        this.video.pause();
    }

    public stop() {
        this.pause();
        this.seekTo(0);
        this.scheduleOnce(this.refreshFrame, 0.1);
    }

    public seekTo(time: number) {
        this.video.currentTime = time;
    }

    public getCurrentTime(): number {
        return this.video.currentTime;
    }

    public getTotalTime(): number {
        return this.video.duration;
    }

    public setLoop(isLoop: boolean) {
        this.isAutoLoop = isLoop;
        this.video.loop = this.isAutoLoop;
    }

    public setMuted(isMuted: boolean) {
        this.isAutoMuted = isMuted;
        this.video.muted = this.isAutoMuted;
    }

    public setPlaybackRate(rate: number) {
        this.video.playbackRate = rate;
    }

    private setVideoEvent() {
        let _this = this;

        this.video.onloadedmetadata = function () {
            cc.log("-> Video Loaded Meta !");
            _this.initEssential();
            _this.emit(_this.evtOnLoadedMeta);
        }

        this.video.onloadeddata = function () {
            cc.log("-> Video Loaded Data !");
            _this.setPreview();
            if (!_this.isAutoPlay)
                _this.pause();
            _this.emit(_this.evtOnLoadedData);
        }

        this.video.onplaying = function () {
            cc.log("-> Video On Playing !");
            _this.emit(_this.evtOnPlaying);
        }

        this.video.onpause = function () {
            cc.log("-> Video On Pause !");
            _this.emit(_this.evtOnPause);
        }

        this.video.onended = function () {
            cc.log("-> Video On Ended !");
            _this.emit(_this.evtOnEnded);
        }
    }

    private initEssential() {
        if (this.isPreview) {
            this.video.currentTime = 0.1;
        }
        this.canvas = document.createElement("canvas");
        this.canvas.width = this.video.videoWidth;
        this.canvas.height = this.video.videoHeight;
        this.setHiddenElement(this.canvas);

        this.context = this.canvas.getContext('2d');
        this.texture = new cc.RenderTexture();
    }

    private setPreview() {
        if (this.isPreview) {
            this.refreshFrame();
        }
    }

    private refreshFrame() {
        this.context.drawImage(this.video, 0, 0, this.canvas.width, this.canvas.height);
        this.texture.initWithElement(this.canvas);
        let spriteFrame: cc.SpriteFrame = new cc.SpriteFrame(this.texture);
        for (let index = 0; index < this.targetSprites.length; index++) {
            this.targetSprites[index].spriteFrame = spriteFrame;
        }
        for (let index = 0; index < this.targetMaterials.length; index++) {
            this.targetMaterials[index].setProperty(DIFFUSE_TEXTURE, this.texture);
        }
    }

    private emit(arrayEvent: Array<Function>) {
        let length = arrayEvent.length;
        for (let index = 0; index < length; index++) {
            arrayEvent[index]();
        }
    }

    private setHiddenElement(element: HTMLElement) {
        element.hidden = true;
        element.style.visibility = "hidden";
        element.style.display = "none";
    }

    private releaseVideoElement() {
        if (this.video != null) {
            this.pause();
            this.video.removeAttribute('src');
            this.video.load();
            this.video.remove();
            this.video = null;
        }
    }

    private releaseCanvasElement() {
        if (this.canvas != null) {
            this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.canvas.remove();
            this.context = null;
            this.canvas = null;
        }
    }

    private releaseTexture() {
        if (this.texture != null) {
            this.texture.clear();
            this.texture.destroy();
            this.texture = null;
        }
    }

    private createVideoElement() {
        this.video = document.createElement('video');
        this.video.controls = false;
        this.video.setAttribute('webkit-playsinline', '');
        this.video.setAttribute("x5-playsinline", '');
        this.video.setAttribute('playsinline', '');
        this.setHiddenElement(this.video);
        this.setVideoEvent();
    }

    start() {
        console.log("-> On Start !");
        this.createVideoElement();
        if (this.videoClips != null && this.videoClips.length > 0) {
            this.load(0);
        }
    }

    update() {
        if (this.isPlaying) {
            this.refreshFrame();
        }
        else {
            this.counter++;
            if (this.counter > 30 && this.context != null) {
                this.counter = 0;
                this.refreshFrame();
            }
        }
    }

    onDisable() {
        cc.log("-> On Disable !");
        this.pause();
    }

    onDestroy() {
        cc.log("-> On Destroy !");
        this.release();
    }
}