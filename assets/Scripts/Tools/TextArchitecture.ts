

const { ccclass, property } = cc._decorator;

interface ITextPiece {
    index: number//開始位置
    text: string
}

@ccclass("TextArchitecture")
export default class TextArchitecture {
    private readonly regex = /<.+?>/g;// 抽取 tag （包含<...>）

    private _charInterval: number = 0.07;
    public get charInterval(): number { return this._charInterval; }
    public set charInterval(v: number) { this._charInterval = v; }


    private parsedTextPiece: ITextPiece[] = null;
    private isShowWhole: boolean = false;

    public showWhole() {
        this.isShowWhole = true;
    }

    public startSpeakingText(txt: string, label: cc.Label | cc.RichText) {
        this.init(txt, label)

        if (label instanceof cc.Label) {
            return this.speakText(txt, label);
        }
        else {
            return this.speakRichText(label);
        }
    }

    private init(txt: string, label: cc.Label | cc.RichText) {
        label.string = "";
        this.isShowWhole = false;

        if (label instanceof cc.RichText) {
            label.string = this.getTagsString(txt);
            this.parsedTextPiece = this.parseRichText(txt);
        }
    }

    private async speakRichText(label: cc.RichText) {
        let pieceId: number = 0;

        while (pieceId < this.parsedTextPiece.length) {
            let piece = this.parsedTextPiece[pieceId];

            let nextChar = piece.text[0];
            piece.text = piece.text.substring(1);

            label.string = this.insertString(label.string, piece.index, nextChar)
            piece.index++;

            let isWholePiece = (piece.text.length <= 0);
            if (isWholePiece) {
                pieceId++;
            }
            await this.delay(this._charInterval);

            if (this.isShowWhole) {
                this.showWholeRichText(label, pieceId);
                pieceId = this.parsedTextPiece.length;
            }
        }
        return Promise.resolve("Rich sentence ended");
    }

    private async speakText(txt: string, label: cc.Label) {
        // cc.log("start:", new Date().toLocaleTimeString());

        let txtArr = txt.split("")
        while (txtArr.length > 0) {
            label.string += txtArr.shift();
            await this.delay(this._charInterval);

            if (this.isShowWhole) {
                this.showWholeText(label, txtArr);
            }
        }

        return Promise.resolve("sentence ended");
    }

    private showWholeRichText(label: cc.RichText, pieceId: number) {
        while (pieceId < this.parsedTextPiece.length) {
            let piece = this.parsedTextPiece[pieceId];
            label.string = this.insertString(label.string, piece.index, piece.text);
            pieceId++;
        }
    }

    private showWholeText(label: cc.Label, txtArr: string[]) {
        label.string += txtArr.join("");
        txtArr.length = 0;
    }

    private delay(timeInSecond: number) {
        return new Promise<void>((resolve) => {
            cc.Canvas.instance.scheduleOnce(() => {
                resolve();
            }, timeInSecond);
        })
    }

    /**回傳text中的tags, 其餘文字去掉*/
    private getTagsString(text: string): string {
        let tagArr: string[] = text.match(this.regex);// 找tag
        let tagStr: string = "";
        if (tagArr) {
            for (const tag of tagArr) {
                tagStr += tag;
            }
        }
        return tagStr;
    }


    private parseRichText(text: string): ITextPiece[] {
        let pureText: string[] = text.split(this.regex);
        let parsedTextPiece: ITextPiece[] = [];

        for (let i = 0; i < pureText.length; i++) {
            const str = pureText[i];

            if (str.length > 0) {
                let id: number = text.indexOf(str);

                let piece: ITextPiece = {
                    index: id,
                    text: str
                }
                parsedTextPiece.push(piece);
            }
        }

        return parsedTextPiece;
    }

    private insertString(str: string, pos: number, insertedStr: string): string {
        let prefix = str.substring(0, pos)
        let affix = str.substring(pos);

        return prefix + insertedStr + affix;
    }
}
