import NENDTool, { AppStoreManager } from "./common/NENDTool";
import Transition from "./common/Transition";


const { ccclass, property } = cc._decorator;

export enum EndcardTypes {
    Normal,
    Force
}

@ccclass
export default class EndcardManager extends cc.Component {

    @property(cc.Node)
    private endcardPage: cc.Node = null;

    @property(cc.Sprite)
    private endcardSprite: cc.Sprite = null;

    @property([cc.SpriteFrame])
    private endcardImgs: cc.SpriteFrame[] = [];

    @property(cc.Node)
    private downloadBtn: cc.Node = null;

    private endcardType: EndcardTypes = EndcardTypes.Normal;

    private isShowed: boolean = false;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {
        this.registerBtnEvent();
        this.coundownEndcard();
    }


    private registerBtnEvent() {
        this.downloadBtn.on(cc.Node.EventType.TOUCH_END, (event: cc.Event.EventTouch) => {
            NENDTool.callAtInteraction("tapDLbutton_endcard_" + EndcardTypes[this.endcardType]);
            AppStoreManager.GoToStore();
            event.stopPropagation();
        }, this);
    }

    public fadeInEndcard(type: EndcardTypes, hideFunc?: Function) {
        if (this.isShowed) return;
        this.isShowed = true;

        Transition.whiteCoverTransition(0.5, () => {
            hideFunc && hideFunc();
            this.showEndcard(type);
        });
    }


    private showEndcard(type: EndcardTypes) {
        cc.log("show endcard : ", EndcardTypes[type]);
        this.endcardType = type;
        this.endcardPage.active = true;
        this.endcardSprite.spriteFrame = this.endcardImgs[type];


        NENDTool.callAtInteraction(EndcardTypes[type] + "_endcard");
        NENDTool.callAtEndOfAd();
    }

    private coundownEndcard(delay:number = 30) {
        // cc.log(new Date())
        this.scheduleOnce(() => {
            // cc.log(new Date())
            this.fadeInEndcard(EndcardTypes.Force);
        }, delay);
    }

}
