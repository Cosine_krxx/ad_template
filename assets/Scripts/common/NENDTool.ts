

export default class NENDTool {
    private static isStartAd: boolean = false;
    private static isEndAd: boolean = false;

    private static isFirstAction: boolean = false;

    public static callAtFirstAction() {
        if (NENDTool.isFirstAction) return;

        NENDTool.isFirstAction = true;
        this.callAtInteraction("first_action")
    }

    public static callAtStartOfAd() {
        if (NENDTool.isStartAd)
            return;

        NENDTool.isStartAd = true;
        if (window.NEND_API) {
            NEND_API.callAtStartOfAd();
        } else {
            cc.warn("\n--> Nend Start Of Ad")
        }
    }

    public static callAtInteraction(tag: string) {
        if (window.NEND_API) {
            NEND_API.callAtInteraction(tag);
        } else {
            cc.warn("\n--> Nend Interaction:", tag)
        }
    }

    public static callAtEndOfAd() {
        if (NENDTool.isEndAd)
            return;

        NENDTool.isEndAd = true;
        if (window.NEND_API) {
            NEND_API.callAtEndOfAd();
        } else {
            cc.warn("\n--> Nend end Of Ad")
        }
    }
}

export class AppStoreManager {
    public static GoToStore() {
        if (window.userClickedDownloadButton)
            userClickedDownloadButton();
        else
            cc.warn("\n--> user Clicked Download Button")
    }
}
