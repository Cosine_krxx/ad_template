// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Hand extends cc.Component {



    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {
        this.node.anchorX = 0.15;  this.node.anchorY = 0.8;

        cc.tween(this.node)
            .then(this.click_Scaled())
            .repeatForever()
            .start();
    }

    // update (dt) {}

    private click_Scaled(): cc.Tween {
        return cc.tween()
            // .set({ anchorX: 0.15, anchorY: 0.8 })
            .to(0.25, { scale: 0.8 })
            .to(0.25, { scale: 1 })
    }

    private click_Rotate(): cc.Tween {
        return cc.tween()
            .to(0.2, { angle: 30 })
            .to(0.2, { angle: 0 })
    }
}
