
const { ccclass, property } = cc._decorator;

export const enum NodeSpace {
    World = 0,
    Local
}

@ccclass
export default abstract class NodeUtility {
    /**
     * 求兩個node插值後的世界座標，這可以用來使node做出非線性的位移
     * @param node 當前node
     * @param target 目標node
     * @param interpolate 插值0~1
     * @param dt Delta Time
     */
    public static lerpNode(node: cc.Node, target: cc.Node, interpolate: number, dt: number): cc.Vec3 {
        let nodePos: cc.Vec3 = this.getWorldPosition(node);
        let targetPos: cc.Vec3 = this.getWorldPosition(target);
        return this.lerp(nodePos, targetPos, interpolate, dt);
    }

    /**
    * 求兩個node插值後的世界座標，這可以用來使node做出非線性的位移
    * @param node 當前node
    * @param target 目標node
    * @param interpolate 插值0~1
    * @param dt Delta Time
    * @example
    *  let pos = KRXX.NodeUtility.lerp(this.greenNode, this.blueNode, 0.5,  dt);
    *  KRXX.NodeUtility.setWorldPosition(this.greenNode, pos);
    */
    public static lerp(nodePos: cc.Vec3, targetPos: cc.Vec3, interpolate: number, dt: number): cc.Vec3 {
        let offset: cc.Vec3 = targetPos.sub(nodePos).multiplyScalar(interpolate).multiplyScalar(dt);
        return nodePos.add(offset);
    }

    public static lerpNumber(num: number, targetNum: number, interpolate: number, dt: number): number {
        let offset: number = (targetNum - num) * interpolate * dt;
        return num + offset;
    }

    public static translate(node: cc.Node, speed: cc.Vec3, dt: number) {
        let pos: cc.Vec3 = this.getWorldPosition(node);
        pos.x = pos.x + speed.x * dt;
        pos.y = pos.y + speed.y * dt;
        pos.z = pos.z + speed.z * dt;
        this.setWorldPosition(node, pos);
    }

    public static setParent(node: cc.Node, parent: cc.Node) {
        let pos: cc.Vec3 = this.getWorldPosition(node);
        node.setParent(parent);
        this.setWorldPosition(node, pos);
    }

    public static getWorldPosition(node: cc.Node): cc.Vec3 {
        let pos: cc.Vec3 = node.position;
        return node.parent.convertToWorldSpaceAR(pos);
    }

    public static getLocalPosition(node: cc.Node): cc.Vec3 {
        return node.position;
    }

    public static setWorldPosition(node: cc.Node, worldPos: cc.Vec3) {
        let localPos = node.parent.convertToNodeSpaceAR(worldPos);
        this.setLocalPosition(node, localPos);
    }

    public static setLocalPosition(node: cc.Node, localPos: cc.Vec3) {
        node.setPosition(localPos);
    }

    public static getLocalScale(node: cc.Node): cc.Vec3 {
        let scale: cc.Vec3 = cc.Vec3.ZERO;
        node.getScale(scale);
        return scale;
    }

    public static setLocalScale(node: cc.Node, scale: cc.Vec3) {
        node.setScale(scale);
    }

    public static getLocalRotation(node: cc.Node): cc.Quat {
        let rot: cc.Quat = new cc.Quat(0, 0, 0, 0);
        node.getRotation(rot);
        return rot;
    }

    /**
     * 設定節點的本地旋轉量
     * 
     * p.s.輸入旋轉量非人類直覺的360度歐拉角，請勿以歐拉角方式輸入new cc.Quat(x, y, z)，您可使用toQuaternion來輸入rot，或setWorldRotationEuler方法來直接以歐拉角方式設定節點旋轉量。
     * 
     * @param node 目標節點
     * @param rot 四元數旋轉量
     */
    public static setLocalRotation(node: cc.Node, rot: cc.Quat) {
        node.setRotation(rot);
    }

    public static getWorldRotation(node: cc.Node): cc.Quat {
        let euler: cc.Vec3 = NodeUtility.toEuler(NodeUtility.getLocalRotation(node)).add(this.getParentsEuler(node));
        return NodeUtility.toQuaternion(euler);
    }

    /**
     * 設定節點的本地旋轉量
     *
     * p.s.輸入旋轉量非人類直覺的360度歐拉角，請勿以歐拉角方式輸入new cc.Quat(x, y, z)，您可使用toQuaternion來輸入rot，或setWorldRotationEuler方法來直接以歐拉角方式設定節點旋轉量。
     *
     * @param node 目標節點
     * @param rot 四元數旋轉量
     */
    public static setWorldRotation(node: cc.Node, rot: cc.Quat) {
        let euler: cc.Vec3 = NodeUtility.toEuler(rot).sub(this.getParentsEuler(node));
        this.setLocalRotation(node, this.toQuaternion(euler));
    }

    public static setWorldRotationEuler(node: cc.Node, euler: cc.Vec3) {
        euler = euler.sub(this.getParentsEuler(node));
        this.setLocalRotation(node, this.toQuaternion(euler));
    }

    public static toQuaternion(euler: cc.Vec3): cc.Quat {
        let rot: cc.Quat = new cc.Quat(0, 0, 0, 0);
        rot.fromEuler(euler);
        return rot;
    }

    public static toEuler(quaternion: cc.Quat): cc.Vec3 {
        let rot: cc.Vec3 = cc.Vec3.ZERO;
        quaternion.toEuler(rot);
        return rot;
    }

    public static instantiate(prefab: cc.Prefab, parent: cc.Node = null): cc.Node {
        if (parent == null) {
            parent = cc.director.getScene();
        }
        let node: cc.Node = cc.instantiate(prefab);
        node.setParent(parent);
        this.setLocalPosition(node, cc.Vec3.ZERO);
        return node;
    }

    public static instantiateType<T extends cc.Component>(type: { prototype: T; }, prefab: cc.Prefab, parent: cc.Node = null): T {
        return this.instantiate(prefab, parent).getComponent(type);
    }

    /**
    * 計算2節點距離的平方
    * @param space default = NodeSpace.World
    */    
    public static getMagnitude(nodeA: cc.Node, nodeB: cc.Node, space: NodeSpace = NodeSpace.World): number {
        if (space == NodeSpace.World) {
            return this.calMagnitude(this.getWorldPosition(nodeA), this.getWorldPosition(nodeB));
        }
        return this.calMagnitude(this.getLocalPosition(nodeA), this.getLocalPosition(nodeB));
    }

    /**
    * 計算2節點的距離
    * @param space default = NodeSpace.World
    */    
    public static getDistance(nodeA: cc.Node, nodeB: cc.Node, space: NodeSpace = NodeSpace.World): number {
        if (space == NodeSpace.World) {
            return this.calDistance(this.getWorldPosition(nodeA), this.getWorldPosition(nodeB));
        }
        return this.calDistance(this.getLocalPosition(nodeA), this.getLocalPosition(nodeB));
    }

    private static calMagnitude(posA: cc.Vec3, posB: cc.Vec3): number {
        let v1 = (posB.x - posA.x);
        let v2 = (posB.y - posA.y);
        let v3 = (posB.z - posA.z);
        return (v1 * v1) + (v2 * v2) + (v3 * v3);
    }

    private static calDistance(posA: cc.Vec3, posB: cc.Vec3): number {
        return Math.sqrt(this.calMagnitude(posA, posB));
    }

    private static getParentsEuler(node: cc.Node): cc.Vec3 {
        let euler: cc.Vec3 = cc.Vec3.ZERO;
        let parent: cc.Node = node.parent;
        while (parent != null) {
            euler = euler.add(NodeUtility.toEuler(NodeUtility.getLocalRotation(parent)));
            parent = parent.parent;
        }
        return euler;
    }
}