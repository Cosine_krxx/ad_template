
const { ccclass, property } = cc._decorator;

@ccclass
export default class Transition extends cc.Component {

    @property(cc.Node)
    private whiteCover: cc.Node = null;

    @property(cc.Node)
    private inputBlocker: cc.Node = null;

    private static _instance: Transition = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.setSingalton();
    }

    public static whiteCoverTransition(delay: number = 0.3, showUpfunc?: Function, finishedFunc?: Function) {
        Transition.stopTrasition();

        let cover: cc.Node = Transition._instance.whiteCover;

        this.setInputBlocker(true);
        cc.tween(cover)
            .set({ active: true, opacity: 0 })
            .delay(delay)
            .to(0.3, { opacity: 255 })
            .call(() => { showUpfunc && showUpfunc(); })
            .delay(0.1)
            .to(0.2, { opacity: 0 })
            .call(() => {
                finishedFunc && finishedFunc();
                this.setInputBlocker(false);
            })
            .start()
    }

    private static stopTrasition() {
        cc.Tween.stopAllByTarget(Transition._instance.whiteCover);

        Transition._instance.whiteCover.opacity = 0;
    }

    public static setInputBlocker(active: boolean) {
        Transition._instance.inputBlocker.active = active;
        cc.log("--> input Blocked:", active)
    }



    private setSingalton() {
        if (Transition._instance == null) {
            Transition._instance = this;
        }
    }

    onDestroy() {
        Transition._instance = null;
    }
}
