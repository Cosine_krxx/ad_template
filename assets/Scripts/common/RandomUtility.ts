
const { ccclass, property } = cc._decorator;

@ccclass
export default abstract class RandomUtility extends cc.Component {

    public static getRandomRange(inMin: number, exMax: number): number {
        return cc.math.randomRange(inMin, exMax);
    }

    public static getRandomRangeInt(inMin: number, exMax: number): number {
        return cc.math.randomRangeInt(inMin, exMax);
    }

    public static getRandomColor(): cc.Color {
        let r: number = this.getRandomRangeInt(0, 256);
        let g: number = this.getRandomRangeInt(0, 256);
        let b: number = this.getRandomRangeInt(0, 256);
        return new cc.Color(r, g, b, 255);
    }

    public static getRandomRangeColor(inMin: number, exMax: number): cc.Color {
        let r: number = this.getRandomRangeInt(inMin, exMax);
        let g: number = this.getRandomRangeInt(inMin, exMax);
        let b: number = this.getRandomRangeInt(inMin, exMax);
        return new cc.Color(r, g, b, 255);
    }
}